#!/usr/bin/env bash

# Items to configure
#
# Mandatory
#
# JENKINS_DUCKDNS_TOKEN - the token for DUCKDNS
# JENKINS_user_email - the email address of the user to register with LetsEncrypt
#
# Optional
#
# FAILURE_ACTION - describe, stop or terminate. What to do with the instance if an error is encountered during script run
#   default "terminate"
# JENKINS_DEBUG - whether to add set -x to the runtime
# JENKINS_SSH_WAIT_TIME - how much time after 'running' state in EC2 is achieved, before trying an SSH connection

set -euo pipefail

main() {
    cd "$(dirname "$0")"
    . ip-ssh.sh # get_instance_ip() and runssh()

    set_variables
    ensure_single_instance
    create_new_instance
    get_instance_ip
    setup_bbb_server
}

set_variables() {
    # Try to access these, if they're not set, error will bubble out.
    : ${JENKINS_DUCKDNS_TOKEN} ${JENKINS_user_email}

    if [[ "$JENKINS_DEBUG" = true ]]; then
        set -x
    fi

    FAILURE_ACTION="${FAILURE_ACTION:-terminate}"

    if [[ ! "${FAILURE_ACTION}" =~ ^(describe|stop|terminate)$ ]]; then
        echo "FAILURE_ACTION=$FAILURE_ACTION - unspported. Specify 'describe', 'stop' or 'terminate'."
    fi

    appname="bigbluebutton_auto"
}

ensure_single_instance() {
    # Find any non-terminated items with the app=bigblutbutton_auto tag
    instance_count="$(aws ec2 describe-instances --filter "Name=tag:app,Values=$appname"|jq ".Reservations[].Instances[].State.Name" -r|grep -vi 'terminated' -c||:)" # Systematically declare success - if no prior instances, grep -v exits with failure

    if [[ "$instance_count" -gt 0 ]]; then
        echo "app=$appname - already running"
        aws ec2 describe-instances --filter "Name=tag:app,Values=$appname"|jq ".Reservations[].Instances[]|(.InstanceId,.State.Name)"
        exit 0
    fi
}

create_new_instance() {
    instance_id="$(aws ec2 run-instances --block-device-mappings 'DeviceName=/dev/sda1,Ebs={DeleteOnTermination=true,VolumeSize=32}' --image-id ami-0b1912235a9e70540 --key-name jenkins --instance-type c5.xlarge --security-group-ids sg-07e279c646c6083fe sg-c317afaa sg-07a46e6e --count 1 --associate-public-ip-address --tag-specifications "ResourceType=instance,Tags=[{Key=app,Value=$appname}]"| jq .Instances[0].InstanceId -r)"

    stop_instance_on_failure() {
        local last_result="$?"
        if [[ "$last_result" != 0 ]]; then
            aws ec2 "$FAILURE_ACTION"-instances --instance-ids "$instance_id"
            exit "$last_result"
        else
            echo "Exited normally."
        fi
    }
    trap stop_instance_on_failure EXIT

    max_count=10
    i=0
    while true; do
        i=$((i+1))

        instance_status="$(aws ec2 describe-instances --instance-ids "$instance_id" |jq .Reservations[].Instances[].State.Name -r)"

        if [[ "$i" -ge "$max_count" ]]; then
            echo "Too long; something is amiss, abort"
            false # trigger exit trap

        elif [[ "$instance_status" != running ]]; then
            echo "Status: $instance_status"
            sleep 5

        else
            break
        fi
    done

    : "'running' means the instance is up, but sshd may not yet be started. give it a little time..."
    sleep "${JENKINS_SSH_WAIT_TIME:-20}"
}

setup_bbb_server() {
    echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDr+8sH7jT85i/+hogiPLmij8FO3NqizIYTnwbs4ayLdii/yc8VyRjPgyg0x/5YRjymcF3NENARzQTjzRPXR1C21UTCGyAtZW5JrPE6CzltN+48L/UbMtt9p3lCx9iOxBjqmY4TUtEqto3/yNrgANDqmCralN9/WzZKAt6GXTBv+rxrQL+rOBfCn90UuDVaiS4MZES8+gccEW7+r+beKn+WtspPaO1zN5FSngfteIgGyrqql0V7if96yjgH+awa1X8vTRiK38qysJbohVttWRyDyz+GVsnrOdwBiEa7Uo7D/cQmafT9E9yncnqkoGJvoOliHtNZRBNIK9KBjD3WZF5z tai@flex"|runssh tee -a .ssh/authorized_keys

    runssh git clone https://gitlab.com/taikedz/cloud_infrastructure.git

    cat <<EOKEYS | runssh tee cloud_infrastructure/duckdns/keys.local.sh
DUCKDNS_TOKEN="${JENKINS_DUCKDNS_TOKEN}"
SUBDOMAIN=b3-tk
EOKEYS
    runssh bash cloud_infrastructure/duckdns/update_ip.sh

    cat <<PRIVDATA | runssh tee cloud_infrastructure/software/bigbluebutton/b3_setup.local.sh
host_fqdn=b3-tk.duckdns.org
user_email="$JENKINS_user_email"
PRIVDATA
    runssh bash cloud_infrastructure/software/bigbluebutton/install.sh

    # ERR_S3_FILE_NOT_FOUND -- it should be an error to fail to download the restore unless otherwise specified
    runssh ERR_S3_FILE_NOT_FOUND="${ERR_S3_FILE_NOT_FOUND:-20}" bash cloud_infrastructure/infrastructure/bigbluebutton/greenlight-backup.sh restore
}

main "$@"
