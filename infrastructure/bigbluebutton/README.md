# BBB instance manager

Create a new BigBlueButton instance on EC2; intended for use on a jenkins server.

Run backups and restores

## Creation - Items to configure

Mandatory

`JENKINS_DUCKDNS_TOKEN` - the token for DUCKDNS
`JENKINS_user_email` - the email address of the user to register with LetsEncrypt, and as admin of BBB
`admin_password` - the default admin password for the admin account

Optional

`FAILURE_ACTION` - stop or terminate. What to do with the instance if an error is encountered during script run
`JENKINS_DEBUG` - whether to add set -x to the runtime

## Backup

The backup script uses an S3 bucket into which the greenlight database is written. It does not attempt to backup recordings, just user settings.

`bash greenlight-backup.sh { backup | restore } [ARCHIVENAME | "-"]`

It must be invoked with the first argument `backup` or `restore` to act accordingly.

The second argument is optional, and allows specifying a custom archive file name. If the argument is `-` then the date is added to the start of the default file name instead.

You can configure the script with an int in the environment variable `ERR_S3_FILE_NOT_FOUND`. This is the exit code to use when the remote S3 file is not found. By default this is `20` as we consider this a failure in the restore process. However if there is reason to assert this is normal, set it to `0`
