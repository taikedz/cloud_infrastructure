#!/usr/bin/env bash

# Configure EC2_ACTION in environment

ERR_TOO_MANY_INSTANCES=10

get_instance_id() {
    instance_id="$(aws ec2 describe-instance --filter "Name=tag:app,Values=bigbluebutton_auto" | jq .Reservations[].Instances[].InstanceId -r)"

    if [[ "$(echo "$instance_id"|grep -Ec '^'||:)" -gt 1 ]]; then
        echo "Multiple instances found !!"
        exit $ERR_TOO_MANY_INSTANCES
    fi
}

main() {
    cd "$(dirname "$0")"
    . ip-ssh.sh

    get_instance_id
    get_instance_ip

    runssh bash cloud_infrastructure/infrastructure/bigbluebutton/greenlight-backup.sh backup
}

take_action() {
    # Always do this on exit, so set in trap
    # (is run in the controller)
    last_exit="$?"

    ec2_action="${ec2_action:-terminate}"
    if [[ "$last_exit" != 0 ]]; then
        ec2_action=stop
    fi

    bash "$HOME/cloud_infrastructure/software/utils/ec2-action-instances.sh" app=bigbluebutton_auto "$ec2_action"
    ec2_action_result="$?"

    echo "EC2 INSTANCE OPERATION USED -----> $ec2_action [-> $ec2_action_result]"
    exit "$last_exit"
}
trap take_action EXIT

main "$@"
