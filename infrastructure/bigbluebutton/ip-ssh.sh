
get_instance_ip() {
    ip_address="$(aws ec2 describe-instances --instance-ids "$instance_id" | jq .Reservations[].Instances[].PublicIpAddress -r)"

    if [[ -z "$ip_address" ]]; then
        echo "No IP address obtained"
        false
    fi
}

runssh() {
    # Normally we'd have to manually confirm we want to connect for the first time, but that defeats automation
    # So we electively do not check host key, we know we just created the server.
    # I can't say I like this solution, but it's all I have for now. --TK
    ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no "ubuntu@${ip_address}" "$@"
}
