#!/usr/bin/env bash

set -euo pipefail

ERR_NO_ACTION=10
ERR_INVALID_ACTION=11
ERR_INVALID_FILE=12
ERR_S3_FILE_NOT_FOUND="${ERR_S3_FILE_NOT_FOUND:-20}"

do_setup() {
    if [[ "${JENKINS_DEBUG:-}" = true ]]; then
        set -x
    fi

    GREENLIGHT_DIR="${GREENLIGHT_DIR:-$HOME/greenlight}"
    GREENLIGHT_TARFILE="b3-tk-greenlight-backup.tar.gz"
    GREENLIGHT_S3="s3://taikedz-backups/bigbluebutton"

    if [[ -z "$*" ]]; then
        echo "No action specified"
        exit $ERR_NO_ACTION

    elif [[ ! "$1" =~ ^(backup|restore)$ ]]; then
        echo "Invalid action"
        exit $ERR_INVALID_ACTION

    else
        MAIN_ACTION="$1"

        if [[ "${2:-}" = - ]]; then
            GREENLIGHT_TARFILE="$(date "+%F--%T"|sed 's/:/./g').$GREENLIGHT_TARFILE"
        else
            GREENLIGHT_TARFILE="${2:-$GREENLIGHT_TARFILE}"
        fi
    fi
}

greenlight:backup() {
    cd "$GREENLIGHT_DIR"
    sudo docker-compose stop
    tar czf "$GREENLIGHT_TARFILE" db/
    sudo docker-compose start

    aws s3 cp "$GREENLIGHT_TARFILE" "$GREENLIGHT_S3/$GREENLIGHT_TARFILE"
}

greenlight:restore() {
    cd "$GREENLIGHT_DIR"
    aws s3 cp "$GREENLIGHT_S3/$GREENLIGHT_TARFILE" "$GREENLIGHT_TARFILE" || exit "$ERR_S3_FILE_NOT_FOUND"

    sudo docker-compose down
    sudo rm -r ./db/
    tar xzf "$GREENLIGHT_TARFILE"
    sudo docker-compose up -d
}

main() {
    do_setup "$@"

    "greenlight:$MAIN_ACTION"
}

main "$@"
