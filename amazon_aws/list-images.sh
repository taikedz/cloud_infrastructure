## List all available Images (not your instances)

echo "This might take a while. It also downloads quite a bit of data (<100MB)"
aws ec2 describe-images > images.local.json

echo "Results written to images.local.json"

# grep '16\.04 LTS' images.local.json |grep -vE 'UNSUPPORTED|Minimal|arm64'|grep 2020-|sort|less
