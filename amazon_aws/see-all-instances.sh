#!/usr/bin/env bash

mkdir -p regions

for region in $(aws ec2 describe-regions --region us-east-1 --output text | cut -f4); do
    echo "Processing region: $region"

    aws ec2 describe-instances --region "$region" > "regions/$region.json"

    jq ".Reservations[].Instances[] | .LaunchTime,.InstanceType,.State.Name,.Tags" < "regions/$region.json" | sed -r 's/^\]/]\n-----------\n/'
done
