# BigBlueButton install

Specification:

* c5.xlarge (4CPU, 8GB)
* 32 GB storage (gp2)
* Ports required
    * 22/tcp
    * 80/tcp , 443/tcp
    * 16384-32768/udp

On Amazon, log in as user "ubuntu"

The recommended storage for production is 500GB

Ideally we'd be able to add additional storage separately for recordings, and then mount it to the appropriate location to dissociate the runtime from the actual recording data...

THen there's the question of how to backup user data etc... not yet looked into
