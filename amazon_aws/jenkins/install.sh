#!/usr/bin/env bash

set -euo pipefail

#FQDN=
echo "Configuring for $FQDN"

sudo apt-get install wget

#wget -q -O - https://pkg.jenkins.io/debian/jenkins.io.key | sudo apt-key add -
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 9B7D32F2D50582E6
echo "deb http://pkg.jenkins.io/debian-stable binary/" | sudo tee /etc/apt/sources.list.d/jenkins.list &>/dev/null

sudo apt-get update

sudo apt-get install openjdk-8-jdk htop tmux git jq awscli jenkins nginx -y

cat <<EOCONF | sudo tee -a /etc/nginx/nginx.conf &>/dev/null
server {
        listen 80;
        server_name $FQDN;

    listen [::]:443 ssl ipv6only=on;
    listen 443 ssl;
    ssl_certificate /etc/letsencrypt/live/$FQDN/fullchain.pem; 
    ssl_certificate_key /etc/letsencrypt/live/$FQDN/privkey.pem; 
    include /etc/letsencrypt/options-ssl-nginx.conf; 
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; 

        location / {
            proxy_pass  http://127.0.0.1:8080;
        }
}

EOCONF

sudo systemctl reload nginx
