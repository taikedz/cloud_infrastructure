#!/usr/bin/env bash

set -euo pipefail

cd "$(dirname "$0")"

# Specify variables:
# - user_email
# - host_fqdn
# - admin_password
. "b3_setup.local.sh"

update_mode=false

is_updating() {
    if [[ -n "$*" ]]; then
        update_mode="$1"
    else
        [[ "$update_mode" = true ]]
    fi
}

check_arguments() {
    if [[ "$*" =~ --update ]]; then
        is_updating true
    elif [[ "$*" =~ --remove ]]; then
        sudo apt-get purge "bbb-*" bigbluebutton
        exit
    fi
}

unpack_letsencrypt() {
    :
    # 1. unpack letsencrypt which should have all files
    # 2. apply patch to nginx config manually to integrate it
}

main() {
    cd "$HOME"
    check_arguments "$@"

    mkdir -p git
    cd git

    if is_updating; then
        if [[ -d bbb-install/.git ]]; then
            (
            cd bbb-install
            git pull
            )
        else
            echo "No bbb-install git - did you move the folder?"
            exit 1
        fi
    else
        [[ -d bbb-install/.git ]] || git clone https://github.com/bigbluebutton/bbb-install
    fi

    cd bbb-install

    greenlight_options=(: $(is_updating || echo " -g")) # When updating, do not re-configure greenlight

    le_options=(-e "${user_email}")
    if [[ -n "${LE_TARBALL:-}" ]]; then
        le_options=(-d)
    fi

    sudo bash bbb-install.sh -s "${host_fqdn}" "${le_options[@]}" "${greenlight_options[@]:1}" -v xenial-22

    if [[ -n "${LE_TARBALL}" ]]; then
        unpack_letsencrypt
    fi

    if ! is_updating; then
        # If not updating, create a new admin user
        #
        # The bbb-install.sh defaults to installing greenlight in the homedir, not sure if this can be overridden.....
        cd "$HOME/greenlight"
        sudo docker exec greenlight-v2 bundle exec rake admin:create["Administrator","${user_email}","${admin_password}","admin"]
    fi
}

main "$@"
