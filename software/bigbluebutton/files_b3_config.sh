files_to_amend=(
    /etc/bigbluebutton/nginx/sip.nginx
    /opt/freeswitch/conf/sip_profiles/external.xml
    /opt/freeswitch/conf/vars.xml
    /opt/freeswitch/etc/freeswitch/sip_profiles/external.xml
    /opt/freeswitch/etc/freeswitch/vars.xml
    /opt/freeswitch/log/freeswitch.xml.fsxml
    /opt/freeswitch/var/log/freeswitch/freeswitch.xml.fsxml
    /usr/local/bigbluebutton/bbb-webrtc-sfu/config/default.yml
    /usr/local/bigbluebutton/core/scripts/bigbluebutton.yml
    /usr/share/bbb-apps-akka/conf/application.conf
    /usr/share/bbb-web/WEB-INF/classes/bigbluebutton.properties
    /usr/share/red5/webapps/sip/WEB-INF/bigbluebutton-sip.properties
    /var/www/bigbluebutton/client/conf/config.xml
)

if [[ "$(basename "$0")" = "files_b3_config.sh" ]]; then
    for item in "${files_to_amend[@]}"; do
        echo "$item"
    done
fi
