# BigBlueButton notes

## Post-install notes

### Error 1020

BBB uses public STUN servers to establish UDP connections with clients for video including webcam and screen sharing. BBB is configured using plain IP addresses, not domain names, so any change in IP of the chosen STUN server may result in loss of this capability. The fix is luckily simple enough:

TL;DR fix:

1. Check for a public STUN server - for example stun.l.google.com:19302
2. Get its IP - `nslookup stun.l.google.com` which gives `108.177.15.127` at the moment
3. Edit `/etc/kurento/modules/kurento/WebRtcEndpoint.conf.ini` on your BBB server
4. Locate and replace the values of `stunServerAddress` and `stunServerPort` with `108.177.15.127` and `19302`
5. Restart BBB : `sudo bbb-conf --restart`

Two public STUN server lists:

* <https://gist.github.com/sagivo/3a4b2f2c7ac6e1b5267c2f1f59ac6c6b>
* <https://gist.github.com/mondain/b0ec1cf5f60ae726202e>

### Change of IP

AWS does not bill full-whack for EC2 instances that are stopped; just their storage. Stopping an EC2 causes its IP address to change however, and this is where problems start...

Using `bbb-conf --setip NEWIP`, some config files are re-written such that where a FQDN should be, the new external IP is placed instead, causing TLS failures when trying to launch a conference room. This is bad.

I wrote my own script for updating IP addresses, based on the config output from `bbb-conf --check` and the manual for setting up the firewall, supplemented with a `grep` to find any mentions of the IP in the filesystem. (checkout commit `4380f74` to see these)

After using the IP reset, status checks out OK with FreeSwtich running; however the output of `bbb-conf --restart` (and `--start`) show these:

```
# Error: Unable to connect to port 1935 (RTMP) on myb3server.duckdns.org
#
# Error: Could not detect FreeSWITCH listening on port 5060
```

This does not work either.

The solution, as it turns out, is to just run the bbb-install script again.

For this purpose, a `--update` flag for the install script was added... and everything is fixed...!

You can automate this in a crontab whenever the server boots up: run `sudo crontab -e` (root's crontab specifically) and add:

```
# IP may have changed, ensure B3 is reconfigured
@reboot cd /home/ubuntu/cloud_infrastructure/software/bigbluebutton; bash install.sh --update
```

You may need to wrap this in a standalone script, and set the PATH correctly (crontab runs with a very pared down path)
