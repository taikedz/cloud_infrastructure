#!/usr/bin/env bash

## Change IP addresses in main config files.
##
## Run as reset-ip.sh IP_DEF ...
##
##    where IP_DEF is "OLDIP:NEWIP"

# Files obtained from the output of
# bbb-conf --setip <newip>
# bbb-conf --check
#
# "bbb-conf --setip" IS NOT for setting a new IP!! It is for setting the public FQDN
#   Furthermore, it leaves a bunch of other files untouched where the IP clearly needs updating.
cd "$(dirname "$0")"

. files_b3_config.sh

validate_ip() {
    declare -n ip_holder="$1"

    if [[ "$ip_holder" =~ ^host: ]]; then
        return
    fi

    local b1 b2 b3 b4
    read b1 b2 b3 b4 < <(echo "$ip_holder" | sed 's/\./ /g')

    bytes=($b1 $b2 $b3 $b4)
    for byte in "${bytes[@]}"; do
        ([[ "$byte" =~ ^[0-9]+$ ]] && [[ $byte -ge 0 ]] && [[ $byte -lt 256 ]]) || { echo "Invalid IP <$ip_holder>"; exit 1; }
    done
    ip_holder="$b1.$b2.$b3.$b4"
}

change_ip() {
    local old_ip
    local new_ip
    old_ip="$1"; shift
    new_ip="$1"; shift

    validate_ip old_ip
    validate_ip new_ip

    echo "$old_ip --> $new_ip"

    sudo sed "s/$old_ip/$new_ip/g" -i "${files_to_amend[@]}"

    sudo bbb-conf --restart
    sudo bbb-conf --status
}

main() {
    local ip_switch

    if [[ "$*" =~ --help ]] || [[ "$*" = "" ]]; then
        grep -E '^##' "$0"
        exit
    fi

    for ip_switch in "$@"; do
        read from_ip to_ip < <(echo "$ip_switch"|sed 's/:/ /')
        change_ip "$from_ip" "$to_ip"
    done
}

main "$@"
