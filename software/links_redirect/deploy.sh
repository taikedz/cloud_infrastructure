#!/usr/bin/env bash

set -euo pipefail

check_args() {
    if [[ "$#" -gt 2 ]] || [[ "$#" -lt 1 ]]; then
        echo -e "\nUsage:\n\t$(basename "$0") FQDN [CERTBOTDIR])\n"
        exit 1
    fi
}

docker_up() {
    if ! sudo docker network ls|grep links_redirect; then
        sudo docker network create links_redirect
    fi

    sudo docker-compose up -d

    sleep 2

    url="http://$(sudo docker inspect links_redirect | jq '.[0].NetworkSettings.Networks.links_redirect.IPAddress' -r)"
}

setup_nginx() {
    sed "s|%FQDN%|$fqdn|g; s|%INTERNAL_URL%|$url|g" nginx/links-redirect.conf | sudo tee /etc/nginx/sites-available/links-redirect.conf
    if [[ ! -e /etc/nginx/sites-enabled/links-redirect.conf ]]; then
        sudo ln -s /etc/nginx/sites-available/links-redirect.conf /etc/nginx/sites-enabled/links-redirect.conf
        sudo systemctl reload nginx
    fi
}

setup_certs() {
    if [[ ! -d "/etc/nginx" ]]; then
        echo "No /etc/nginx"
        exit 1
    fi

    if [[ -n "$certbotdir" ]] && [[ -d "$certbotdir" ]] && [[ -n "$fqdn" ]]; then
        (
            cd "$certbotdir"
            ./certbot-auto -d "$fqdn"
        )
    fi
}

set_default_config() {
    if [[ ! -e "./etc/mappings.local.json" ]]; then
        mkdir -p ./etc
        # ./etc may be owned by the container's root, so we need to use host root to write into it
        echo '{}' | sudo tee "./etc/mappings.local.json" >/dev/null
    fi
}

main() {
    fqdn="$1"
    certbotdir="${2:-}"

    check_args "$@"
    setup_certs

    cd "$(dirname "$0")"

    docker_up
    setup_nginx

    set_default_config
}

main "$@"
