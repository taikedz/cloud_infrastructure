#!/usr/bin/env bash

HERE="$(dirname "$0")"
phpl_ver="8.0"

docker run --rm -t -v "${PWD}":/workdir "overtrue/phplint:$phpl_ver" "$HERE/www"  --exclude=vendor
