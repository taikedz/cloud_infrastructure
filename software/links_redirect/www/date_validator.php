<?php
function validate_activation_date($link_profile) {
    $activation_date = $link_profile['activate'] ?? null ;

    if( ! is_in_time($activation_date) ) {
        display_message($link_profile['name'],
            "The requested link ${link_profile['name']} is not active at this time. Please check back later :-)");
        die();
    }
}

function is_in_time($activation) {
    $t_start = $activation['start'] ?? null;
    $t_end   = $activation['end'] ?? null;

    if(in_array(null, [$t_start, $t_end])) {
        if($t_start == $t_end) // Both null, was not set, allow activation
            { return true; }
        else // One set but not the other
            { return false; }
    }

    $t_start = strtotime($t_start);
    $t_end   = strtotime($t_end);

    $now = new DateTime();
    $now = $now->getTimestamp();

    return $t_start <= $now && $now < $t_end ;
}

?>
