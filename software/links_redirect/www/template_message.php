<html>
<head>
<title><?= $message_title ?></title>
<link rel="stylesheet" href="main.css" />
<?php

if($add_timer) {
?>
<script type="text/javascript">

minutes = 1;

setTimeout(function() {
    document.getElementById("reload_notifier").setInnerText("Reloading ...");
    window.location = window.location.href;
}, minutes*60*1000);

</script>
<?php
}

?>
</head>
<body>

<p id="message"><?= $message_text ?></p>

<p id="reload_notifier"></p>

</body></html>
