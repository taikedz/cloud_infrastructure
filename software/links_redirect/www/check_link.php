<?php

/* A simple script - check a config for a series of URL definitions against slugs in JSON
 * If the slug does not map a URL, fail
 * If the slug's definition has an activation date, activate it on that date only (server's time)
 * Else tell the use to come back later.
 *
 */

require_once "link_mapper.php";
require_once "date_validator.php";


function main() {
    script_setup();

    $url_mappings = load_mappings();
    $link_profile = get_link_profile($url_mappings);

    if($link_profile != null) {
        try {
            validate_activation_date($link_profile);
            execute_redirection($link_profile);

        } catch(exception $e) {
            display_message("URL Check Error", "There was an error in checking the URL '${link_profile['url']}'.");
        }
    } else {
        display_message("URL error", "Invalid requested URL / no URL requested.");
    }
} // main


function script_setup() {
    set_error_handler("error_suppressant");

    // Prevent caching results
    header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");
}


function execute_redirection($link_profile) {
    $result = get_url($link_profile['url']);

    if ($result) {
        $theurl = $link_profile['url'];
        header('Location: '.$link_profile['url']);

        display_message($link_profile['name'],
           "Redirecting to <a href=\"$theurl\">$theurl</a>...");
    } else {
        display_message($link_profile['name'],
            "The URL you requested to '${link_profile['name']}' is not currently online. Please check back later :-)",
            true);
    }
}


function display_message($message_title, $message_text, $add_timer = false) {
    $message_title = "Redirector : $message_title";
    include "template_message.php";
}


function get_url($url) {
    $resource = curl_init($url);

    curl_setopt($resource, CURLOPT_CONNECTTIMEOUT, 10);
    curl_setopt($resource, CURLOPT_RETURNTRANSFER, true);

    return curl_exec($resource);
}


function error_suppressant($errno, $errmsg) {
    //print("<!-- $errmsg -->");
    return true;
}

main();

?>
