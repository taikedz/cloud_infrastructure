<?php
function load_mappings() {
    $mappings_file="/etc/linkchecker/mappings.local.json";
    return json_decode(file_get_contents($mappings_file) , true);
}


function get_link_profile($url_mappings) {
    $link_profile = null;
    foreach ($url_mappings as $rid => $link_def) {
        if($rid == $_GET['id']) {
            $link_profile = $url_mappings[$rid];
            break;
        }
    }

    return $link_profile;
}
?>
