# URL Redirect with link checks

Stub for a link redirection site. The primary purpose of this tool is to provide a destination to share BBB urls for when it is in a "down" state (cost saving).

## Setup

### Configuration

A JSON file of mappings must be provided in `./etc/mappings.local.json` in this format:

```json
{
    "url-id-string": {
        "name": "Title of the URL",
        "url": "http://www.example.com"
    },

    ...

}
```

### Deployment

Run Let's Encrypt to get the appropriate SSL certificate if desired, or remove the SSL portions

* Manual install
    * Put `www/` in your `/var/www/html` or equivalent, and `etc/mappings.local.json` into your `/etc/linkchecker/mappings.local.json`
    * You can then deploy `./etc/links-redirect.conf` to your `/etc/nginx/sites/available` directory, replacing the placeholders as required, and setting the symlink appropriately.
* Docker compose
    * You can use `docker-compose up -d` when in the current directory to bring up an Apach+PHP server with the files in the right place

A script to do the docker-based deployment is available in `deploy.sh`

## Features

### Current features

When accessed with `?id=LINKID`, the ID is looked up against a file mapping of URLs. The URL is checked, and if it is online, the redirect is performed.

If not online, an error message is printed. The page returned when a site is offline will auto-refresh every minute.

### Future features

The tool can be configured to access a database instead of a mapping file.

When accessed without `?id=LINKID`, a search box is provided. When accessed with `?search=SEARCHSTRING`, will filter out on the names of the links, and display them, along with their online state. A configurable maximum number of results to return must be set to not overload the queries for display. A hard maximum of `10` is suggested.
