#!/usr/bin/env bash

set -euo pipefail

printhelp() {
        sn="$(basename "$0")"
        echo "$sn {KEYNAME=KEYVALUE | INSTANCE_ID} [ACTION]"
        echo
        echo "Perform ACTION on an existing instance, filtered by a KEYNAME and KEYVALUE, where the value is assigned to the specified key - e.g. '$sn name=apache2 stop'"
        echo "or - perform ACTION on an existing instance identified by INSTANCE_ID - e.g. '$sn i-vsd98a789f8 terminate'"
        echo "ACTION can be describe, start, stop or terminate (by default, 'describe')"
}

main() {
    if [[ "$*" =~ --help ]] || [[ -z "$*" ]]; then
        printhelp
        exit
    fi

    # ---
    local operation="${2:-describe}"

    if [[ ! "$operation" =~ ^describe|start|stop|terminate$ ]]; then
        echo "Unknown operation <$operation>. Supported operations: describe, start, stop, terminate"
        exit 1
    fi

    # ---
    if [[ "$1" =~ '=' ]]; then
        local tag_name="${1%%=*}"
        local tag_value="${1#*=}"
        local instance_ids=($( (set -x; aws ec2 describe-instances --filter "Name=tag:$tag_name,Values=$tag_value") | jq ".Reservations[].Instances[].InstanceId" -r))

    elif [[ "$1" =~ ^i- ]]; then # Assumes that a valid instance ID necessarily starts with "i-" - is this true?
        local instance_ids=("$1")

    else
        echo "Invalid filter '$1'"
        printhelp
        exit 1
    fi

    # ---
    if [[ -n "${instance_ids[*]}" ]]; then
        (set -x
            aws ec2 "${operation}"-instances --instance-ids "${instance_ids[@]}"
        )
    else
        echo "No instances match that selector."
    fi

}

main "$@"
