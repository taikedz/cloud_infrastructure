IMAGE_ID="ami-04e90e551afdc4b7c" # Windows 2016 base
KEY_NAME=flex # Add the name of a keypair here, this will be used to encrypt the Admin password which you get from AWS web console
INSTANCE_TYPE="t2.large" # Windows is a hungry beast. Feed it well.
EBS_STRING="DeviceName=xvdc,Ebs={DeleteOnTermination=true,VolumeSize=32}" # The primary hard drive, xvd* will map into c drive
SECURITY_GROUPS="sg-0f0a51a61e23a628a" # Add your security group IDs, space-separated
TAG_SPECS="{Key=work,Value=windows}"
