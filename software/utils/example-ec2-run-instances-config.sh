IMAGE_ID="ami-0b1912235a9e70540" # Canonical, Ubuntu, 16.04 LTS, amd64 xenial image build on 2020-04-07
KEY_NAME=flex # Add the name of a keypair here
INSTANCE_TYPE="t2.micro"
EBS_STRING="DeviceName=/dev/sda1,Ebs={DeleteOnTermination=true,VolumeSize=8}" # The primary hard drive
SECURITY_GROUPS="" # Add your security group IDs, space-separated
TAG_SPECS="{Key=app,Value=app_name}"
