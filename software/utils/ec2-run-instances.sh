set -euo pipefail

if [[ -z "$*" ]]; then
    echo "Specify a config file to read from, and optionally a user-data script file."
    exit 1
fi

. "$1"
userdata=(:)
if [[ -n "${2:-}" ]]; then
    userdata=(: --user-data "$2")
fi

result_file="$(mktemp ./result-ec2-run-instances-XXXX.local.json)"

params=(
    --block-device-mappings "$EBS_STRING"
    --image-id "$IMAGE_ID"
    --key-name "$KEY_NAME"
    --instance-type "$INSTANCE_TYPE"
    --security-group-ids $SECURITY_GROUPS # Deliberately non-quoted
#    --subnet-id "$SUBNET_ID"
    --count 1
    --associate-public-ip-address
    --tag-specifications "ResourceType=instance,Tags=[$TAG_SPECS]"
    "${userdata[@]:1}"
)

echo "Launching EC2 instance"
aws ec2 run-instances "${params[@]}" | tee "$result_file"
echo "Results saved to $result_file ."

if which jq &>/dev/null; then
    instance_id="$(jq ".Instances[0].InstanceId" "$result_file" -r)"
    echo "Once the instance is launched, you can find the public IP with this command:"
    echo "aws ec2 describe-instances --instance-ids '$instance_id'|jq .Reservations[0].Instances[0].PublicIpAddress"
fi
