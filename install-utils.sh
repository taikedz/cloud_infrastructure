#!/usr/bin/env bash

scripts_to_install=(
    b64tgz
    ec2-run-instances.sh
    ec2-action-instances.sh
)

localbin_d="$HOME/.local/bin"

update_path=false

if [[ "$*" =~ --all ]]; then
    sudo apt update && sudo apt install -y tmux htop git wget jq awscli
fi

if ! [[ "$PATH" =~ ":$HOME/.local/bin" ]] ; then
    update_path=true
fi

if [[ ! -d "$localbin_d" ]]; then
    mkdir -p "$localbin_d"
fi

if [[ "$update_path" = true ]]; then
    echo 'export PATH="$PATH:$HOME/.local/bin" ' >> "$HOME/.bashrc"
fi

cd "$(dirname "$0")/software/utils"

for install_target in "${scripts_to_install[@]}"; do
    (set -x
    cp "$install_target" "$localbin_d/"
    )
done

if [[ "$update_path" = true ]]; then
    echo "Updated \$PATH in .bashrc ; please run 'exec bash' to reload."
fi
