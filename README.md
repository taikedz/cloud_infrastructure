# Cloud infrastructure projects

Some infrastructure items and learning notes.

## `amazon_aws/`

Some deployment notes relevant to what's needed on AWS platform, and example definitions to be used with the `ec2-*` utils in `software/utils`

## `software/utils`

There are some utility scripts shipped here, run `bash install-utils.sh` to set them up.

You can run `bash install-utils.sh --all` to also install supporting tools from the repositories, like `awscli` and `jq`.

## `*.local.*`

Some scripts require some private information at runtime; these are specified in files matching the `*.local.*` file name glob pattern.

You can place such files on the server for running without the risk of them interfering with git-pull updates etc, or being added to the repository.

# Example

Say we want a BigBlueButton server, on AWS using an EC2 instance.

Ensure you have an IAM in your AWS account, and have run `aws configure` for access.

* Run the `amazon_aws/ecs-run-instances.sh` script against a configuration file you constructed
    * There is an example file with partly-populated parameters
* SSH to the newly set up instance

* Install the utils
* Update DuckDNS
    * Create a `keys.local.sh` in ./duckdns/ to configure `DUCKDNS_TOKEN` and `SUBDOMAIN`
    * Run the DuckDNS script to update the reserved name's IP to the new server
    * Run the `set-cron.sh` script to ensure the domain is updated after every reboot
* Setup BigBlueButton
    * Create `software/bigbluebutton/b3_setup.local.sh` to configure `host_fqdn` and `user_email`
    * Run the installer in `software/bigbluebutton/install.sh`
* Log on to the server as admin and change the password immediately

You can replace the default presentation globally by replacing `/var/www/bigbluebutton-default/default.pdf`
