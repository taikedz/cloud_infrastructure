#!/usr/bin/env bash

cd "$(dirname "$0")"

sudo crontab -l > crontabfile

if ! grep -q update_ip.sh crontabfile; then
    echo "@reboot cd '$PWD'; bash update_ip.sh" >> crontabfile
    sudo crontab crontabfile
fi

rm crontabfile
