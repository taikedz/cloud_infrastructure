# Updating IP

If the server's IP changes after powering off, like on AWS, you can ensure DuckDNS knows about it by adding this to root's crontab (`sudo crontab -e`)

```
# IP may will have changed, tell DuckDNS about it
@reboot cd /home/ubuntu/cloud_infrastructure/duckdns; bash update_ip.sh
```
