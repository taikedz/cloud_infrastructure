#!/usr/bin/env bash

set -euo pipefail

. "$(dirname "$0")/keys.local.sh"

url="https://www.duckdns.org/update?domains=${SUBDOMAIN}&token=${DUCKDNS_TOKEN}&ip="

wget "$url" -O - &>> "./duckdns.local.log"
